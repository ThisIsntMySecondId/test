module.exports = {
    strip_parameters: ['utm_source', 'utm_medium', 'utm_campaign', 'admitad_uid', 'tagtag_uid', 'ie', 'linkCode', 'tag', 'ascsubtag', 'linkId', 'bbn', 'offer_id', 'aff_id', 'aff_click_id', 'p', 'affExtParam1', 'affExtParam2'],
    trim_words: 'utm_source',
}