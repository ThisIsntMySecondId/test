const mysql = require('mysql2/promise');
const puppeteer = require('puppeteer');
const axios = require('axios');
const config = require('./config');
const Url = require('url');
const QueryString = require('querystring');

async function main() {
    const connection = await mysql.createConnection(config.db);
    const [rows, fields] = await connection.execute('SELECT *, params->"$.affiliate_link" as affiliate_link, params->"$.id" as pid FROM `node_jobs` ORDER BY id LIMIT 10 OFFSET 50');
    
    let links = await Promise.all(rows.map(processLinks));
    console.log(links);
    
    connection.end(function (err) {
        console.log('conn ended');
    });
}

main();

const processLinks = async (row) => {
    const finalLink = await getFinalLink(row.affiliate_link);
    const cleanedLink = await removeExtraQueryParam(finalLink);
    const finalLinkStatus = await validateLink(cleanedLink);
    return await {
        rowId: row.id,
        couponId: row.pid,
        finalLinkStatus,
        finalLink: await getFinalLink(row.affiliate_link),
        cleanedLink
    }
}

const validateLink = async (link) => {
    try {
        let res = await axios.get(link);
        return res.status;
    } catch (e) {
        return error.response.status;
    }
}

const getFinalLink = async (url) => {
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitForTimeout(2000);
    let result = await page.url();
    browser.close();
    return result;  
};

const removeExtraQueryParam = (link) => {
    const linkWithoutUtms = link.split(config.linkConfigs.trim_words)[0];
    const parsedLink = Url.parse(linkWithoutUtms, true);
    const baseLink = link.split('?')[0];
    const newQueryParams = QueryString.stringify(Object.fromEntries(Object.entries(parsedLink.query)
        .filter(param => !config.linkConfigs.strip_parameters.includes(param[0]))));
    return baseLink + '?' + newQueryParams;   
}
// let c = removeExtraQueryParam('https://www.myntra.com/handbags?ie=qwe&f=Gender%3Amen+women%2Cwomen&rf=Discount+Range%3A40.0_100.0_40.0+TO+100.0&utm_source=admitad&utm_medium=affiliate&utm_campaign=c9cd2c69ddf51c579385129bbe0ac4f1&admitad_uid=c9cd2c69ddf51c579385129bbe0ac4f1&tagtag_uid=c9cd2c69ddf51c579385129bbe0ac4f1');